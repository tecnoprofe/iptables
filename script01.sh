# Ing. Jaime Zambrana Chacón PhD.
# Configuración de firewall iptables en ubuntu.

echo Iniciando configuracion...

# Primero borramos todas las reglas existentes
iptables -F
iptables -X
iptables -Z
iptables -t nat -F

# Al firewall tenemos acceso desde la red local
iptables -A INPUT -s 192.168.0.0/24 -i eth0 -j ACCEPT

##Puertos a abrir (aquí pondremos una línea por cada puerto a abrir)

#Apache (servidor web)
iptables -A INPUT -s 0.0.0.0/0 -p tcp --dport 80 -j ACCEPT

#MySQL
iptables -A INPUT -s 0.0.0.0/0 -p tcp --dport 3306 -j ACCEPT

#SSH
iptables -A INPUT -s 0.0.0.0/0 -p tcp --dport 22 -j ACCEPT

#FTP
iptables -A INPUT -s 0.0.0.0/0 -p tcp --dport 21 -j ACCEPT

# bloquear paquetes ICMP
iptables -A INPUT -p icmp --icmp-type echo-request -j DROP 
